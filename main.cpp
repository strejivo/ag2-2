#include <cstdlib>
#include <string.h>
#include <iostream>
#include <queue>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <climits>

using namespace std;

struct node{
    map<unsigned int, unsigned int> neibours;
};

unsigned int xMax, yMax, zMax, minX = 0, maxX = 0, minY = 0, maxY = 0, minZ = 0, maxZ = 0;
node * adjMatrix;
unsigned int matrixSize;

void printPointByIndex(unsigned int index) {
    unsigned int x, y, z;
    x = index / ((yMax + 2)*(zMax + 2));
    y = (index - x * ((yMax + 2)*(zMax + 2))) / (zMax + 2);
    z = (index - x * ((yMax + 2)*(zMax + 2))- (y * (zMax + 2)));
    cout << x + minX - 1 << " " << y + minY - 1 << " " << z + minZ - 1 << endl;
}

unsigned int bfSearch(node * m_Graph, unsigned int m_Size, unsigned int m_Start, unsigned int m_End, bool print = false) {
    unsigned int maxFlow = 0;
    unsigned int flow = UINT_MAX;
    bool * visited = new bool [m_Size];
    for (unsigned int i = 0; i < m_Size; i++) visited[i] = false;
    unsigned int * m_Parents = new unsigned int [m_Size];
    queue < unsigned int > q;
    q.push(m_Start);
    visited[m_Start] = true;
    bfs: while(!q.empty()){
        unsigned int wrk = q.front();
        q.pop();
        for(auto it : adjMatrix[wrk].neibours){
            if(!visited[it.first]){
                m_Parents[it.first] = wrk;
                if(it.first == m_End){
                    memset(visited, false, sizeof(bool)*m_Size);
                    unsigned int x, tmp;
                    for(x = m_End; x != m_Start; x=tmp){
                        tmp = m_Parents[x];
                        flow = min(flow, m_Graph[tmp].neibours[x]);
                    }
                    for(x = m_End; x != m_Start; x=tmp){
                        tmp = m_Parents[x];
                        m_Graph[tmp].neibours[x] -= flow; 
                        if(!m_Graph[tmp].neibours[x]) m_Graph[tmp].neibours.erase(x);
                        m_Graph[x].neibours[tmp] += flow;
                    }
                    maxFlow += flow;
                    flow = INT_MAX;
                    while(q.size()) q.pop();
                    q.push(m_Start);
                    visited[m_Start] = true;
                    goto bfs;
                }
                q.push(it.first);
                if(print) printPointByIndex(it.first);
                visited[it.first] = true;
            }
        }
    }
    delete [] visited;
    delete [] m_Parents;
    return maxFlow;
}

struct point {

    point() {
    };

    point(unsigned int x, unsigned int y, unsigned int z) :
    x(x), y(y), z(z) {
    }
    unsigned int x;
    unsigned int y;
    unsigned int z;

    bool operator==(point X) {
        if (X.x == x && X.y == y && X.z == z) return true;
        else return false;
    }

    point operator-(const point & a) const {
        return point(x - a.x, y - a.y, z - a.z);
    }
};
vector<point> covers;

bool isInside(unsigned int minX, unsigned int maxX, unsigned int minY, unsigned int maxY, unsigned int minZ, unsigned int maxZ, unsigned int X, unsigned int Y, unsigned int Z) {
    if (X >= minX && X <= maxX && Y >= minY && Y <= maxY && Z >= minZ && Z <= maxZ) return true;
    return false;
}

size_t node_x, node_y, node_z;

unsigned int getPos(point x) {
    return x.x * (yMax + 2)*(zMax + 2) + x.y * (zMax + 2) + x.z;
}

unsigned int getPos(unsigned int x, unsigned int y, unsigned int z) {
    return x * (yMax + 2)*(zMax + 2) + y * (zMax + 2) + z;
}

int main() {
    cin >> xMax >> yMax >> zMax;
    //Do something
    vector<point> * rads = new vector<point>, * covers = new vector<point>;
    unsigned int radCnt, covCnt, tmpX, tmpY, tmpZ;
    cin >> radCnt;
    for (unsigned int i = 0; i < radCnt; i++) {
        cin >> tmpX >> tmpY>>tmpZ;
        minX = (minX == 0 ? tmpX : min(minX, tmpX));
        minY = (minY == 0 ? tmpY : min(minY, tmpY));
        minZ = (minZ == 0 ? tmpZ : min(minZ, tmpZ));
        maxX = max(maxX, tmpX);
        maxY = max(maxY, tmpY);
        maxZ = max(maxZ, tmpZ);
        rads->push_back(point(tmpX, tmpY, tmpZ));
    }
    cin>>covCnt;
    for (unsigned int i = 0; i < covCnt; i++) {
        cin >> tmpX >> tmpY>>tmpZ;
        if (isInside(minX, maxX, minY, maxY, minZ, maxZ, tmpX, tmpY, tmpZ)) covers->push_back(point(tmpX, tmpY, tmpZ));
    }
    point beg(minX - 1, minY - 1, minZ - 1);
    xMax = maxX - minX + 1;
    yMax = maxY - minY + 1;
    zMax = maxZ - minZ + 1;
    matrixSize = (xMax + 2)*(yMax + 2)*(zMax + 2) + 2;
    adjMatrix = new node [matrixSize];
    for (unsigned int i = 0; i < matrixSize - 2; i++) {
        if (i > 0) adjMatrix[i].neibours[i - 1] = 1;
        if (i + 1 < matrixSize - 2) adjMatrix[i].neibours[i + 1] = 1;
        if (i >= zMax + 2) adjMatrix[i].neibours[i - (zMax + 2)] = 1;
        if (i + zMax + 2 < matrixSize - 2) adjMatrix[i].neibours[i + zMax + 2] = 1;
        if (i >= (yMax + 2)*(zMax + 2)) adjMatrix[i].neibours[i - (yMax + 2)*(zMax + 2)] = 1;
        if (i + (yMax + 2)*(zMax + 2) < matrixSize - 2) adjMatrix[i].neibours[i + (yMax + 2)*(zMax + 2)] = 1;
    }
    unsigned int index;
    for (unsigned int i = 0; i < 2; i++) {
        for (unsigned int j = 0; j < xMax + 2; j++) {
            for (unsigned int k = 0; k < yMax + 2; k++) {
                index = getPos(j, k, i * (zMax + 1));
                adjMatrix[index].neibours[matrixSize - 1] = adjMatrix[matrixSize - 1].neibours[index] = 1;
            }
            for (unsigned int l = 0; l < zMax + 2; l++) {
                index = getPos(j, i * (yMax + 1), l);
                adjMatrix[index].neibours[matrixSize - 1] = adjMatrix[matrixSize - 1].neibours[index] = 1;

            }
        }
        for (unsigned int k = 0; k < yMax + 2; k++) {
            for (unsigned int l = 0; l < xMax + 2; l++) {
                index = getPos(l, k, i * (zMax + 1));
                adjMatrix[index].neibours[matrixSize - 1] = adjMatrix[matrixSize - 1].neibours[index] = 1;

            }
            for (unsigned int l = 0; l < zMax + 2; l++) {
                index = getPos(i * (xMax + 1), k, l);
                adjMatrix[index].neibours[matrixSize - 1] = adjMatrix[matrixSize - 1].neibours[index] = 1;
            }
        }
        for (unsigned int k = 0; k < zMax + 2; k++) {
            for (unsigned int l = 0; l < yMax + 2; l++) {
                index = getPos(i * (xMax + 1), l, k);
                adjMatrix[index].neibours[matrixSize - 1] = adjMatrix[matrixSize - 1].neibours[index] = 1;

            }
            for (unsigned int l = 0; l < xMax + 2; l++) {
                index = getPos(l, i * (yMax + 1), k);
                adjMatrix[index].neibours[matrixSize - 1] = adjMatrix[matrixSize - 1].neibours[index] = 1;
            }
        }
    }
    for (auto it : *rads) {
        adjMatrix[matrixSize - 2].neibours[getPos(it - beg)] = adjMatrix[getPos(it - beg)].neibours[matrixSize - 2] = 7;
    }
    for (auto it : *covers) {
        adjMatrix[getPos(it - beg)].neibours[matrixSize - 1] = adjMatrix[matrixSize - 1].neibours[getPos(it - beg)] = 6;
    }
    delete rads;
    delete covers;
    cout << bfSearch(adjMatrix, matrixSize, matrixSize - 2, matrixSize - 1) << endl;
    bfSearch(adjMatrix, matrixSize, matrixSize - 2, matrixSize - 1, true);
    //cleanup
    delete [] adjMatrix;
}